#!/bin/python3

import time
from tkinter import *
from tkinter.messagebox import *
from datetime import date
import os

import board
import neopixel
pixels = neopixel.NeoPixel(board.D18, 18)

#initilisation des LEDS
pixels[0] = (0, 0, 255)
time.sleep(0.1)
pixels[1] = (0, 0, 255)
time.sleep(0.1)
pixels[2] = (0, 0, 255)
time.sleep(0.1)
pixels[3] = (0, 0, 255)
time.sleep(0.1)
pixels[4] = (0, 0, 255)
time.sleep(0.1)
pixels[5] = (0, 0, 255)
time.sleep(0.1)
pixels[6] = (0, 0, 255)
time.sleep(0.1)
pixels[7] = (0, 0, 255)
time.sleep(0.4)
pixels[8] = (0, 0, 255)
time.sleep(0.2)
pixels[9] = (0, 0, 255)
time.sleep(0.1)
pixels[10] = (0, 0, 255)
time.sleep(0.1)
pixels[11] = (0, 0, 255)
time.sleep(0.1)
pixels[12] = (0, 0, 255)
time.sleep(0.1)
pixels[13] = (0, 0, 255)
time.sleep(0.1)
pixels[14] = (0, 0, 255)
time.sleep(0.1)
pixels[15] = (0, 0, 255)
time.sleep(0.1)
pixels[16] = (0, 0, 255)
time.sleep(0.4)
pixels[17] = (0, 0, 255)
time.sleep(0.4)
pixels.fill((0, 0, 0))

def init_led():
    led_init=1
    while led_init <= 18:
        led_init = str(led_init)
        test_init = open(led_init , "r")
        test = test_init.read()
        led_plus_un = int(led_init)
        led = led_plus_un - 1
        if test == "" :
            pixels[led] = ( 0, 50, 0)
        if test != "" :
            pixels[led] = ( 50, 0, 0)
        led_init = int(led_init)
        led_init += 1
        

init_led()

# Définition de la fonction initialisation/reinitialisation du programe complet
def initialisation():
    if askyesno('Attention', 'Souhaitez vous vraiment réinitialiser le programme ? Tout les prêts enregistrés seront perdus.'):
        n1=1
        while n1<=18:
            n1 = str (n1)
            cr1 = open (n1, "w")
            cr1.write("")
            cr1.close()
            n1 = int (n1)
            n1 += 1

        showinfo('OK', 'Le programme est réinitialisé')

    else:
        showinfo('Ok', 'C\'est plus prudent !')

def fonc_ret_ma1():
    def retour() :
        no = str(Retour_ma1.get())
        verif = open(no, "r")
        dejapresent=verif.read()
        verif.close()
        if dejapresent != "":
            if askyesno('Confirmer', 'Voulez vous vraiment restituer la clé Thirard n° ' + Retour_ma1.get() + ' ?'):
                no = str(Retour_ma1.get())
                raz_etat = open(no, "w")
                raz_etat.write("")
                raz_etat.close()
                clignoled = int(no)
                clignoled = clignoled - 1
                pixels[clignoled] = (0, 0, 255)
                time.sleep(0.5)
                pixels[clignoled] = (0, 0, 0)
                time.sleep(0.5)
                pixels[clignoled] = (0, 0, 255)
                time.sleep(0.5)
                pixels[clignoled] = (0, 0, 0)
                time.sleep(0.5)
                init_led()
                showinfo('OK', 'Clé n° '+ no +' restituée')
            else :
                showinfo('Annulé', 'Annulé')

        if dejapresent == "":
            showinfo('Erreur',"Clé déjà en stock")

        fenetre_retour_ma1.destroy()

    fenetre_retour_ma1= Toplevel()
    fenetre_retour_ma1.title("Retour de clé Thirard") # Titre de la fenetre

    # Champ entrée du n° de moyen d'accès
    cpRetour = Label(fenetre_retour_ma1, text = 'Quelle clé souhaitez vous restituer ? ')
    cpRetour.grid(column=0, row=1, sticky='w')
    Retour_ma1=StringVar()
    Champ = Entry(fenetre_retour_ma1, textvariable= Retour_ma1, width=31)
    Champ.grid(column=1, row=1, sticky='sw', columnspan=2, padx=10)

    Valider= Button (fenetre_retour_ma1, text="Valider", command=retour, pady=2)
    Valider.grid (row=2, column=0,columnspan=3, sticky='ew',pady=20)
    fenetre_retour_ma1.mainloop()



def nouveau():

    #Création de la fonction effacer/reinitialiser le formulaire
    def effacer():
        Champ.delete(0,END)             #Nom
        Champ2.delete(0,END)            #Prénom
        Champ3.delete(0,END)            #Entreprise
        Champ4.delete(0,END)            #Tel
        Champ5.delete(0,END)            #E-mail
        Champ6.delete(0,END)            #Poste
        Champ7.delete(0,END)            #Commentaires
        C1.deselect()                   #Moyen d'accès 1
        C2.deselect()                   #Moyen d'accès 2
        C3.deselect()                   #Moyen d'accès 3
        homme.deselect()
        femme.deselect()

    #Création de la fonction envoyer
    def envoyer():


        if (sex.get()==0):
            titre = "M."
        elif (sex.get()==1):
            titre = "Mme"

        #Recherche de clé disponible pour moyen d'accès n° 1

        recherche_cle_dispo = 1
        while recherche_cle_dispo <= 18:
            recherche_cle_dispo = str(recherche_cle_dispo)
            fichier_test = open(recherche_cle_dispo , "r")
            test = fichier_test.read()
            if test == "" : #Si une clé est dispo
                ma1_def = str(recherche_cle_dispo)
                fichier_maj = open(ma1_def, "w")
                fichier_maj.write("0")
                fichier_maj.close()
                fichier_maj = open(ma1_def, "w")
                fichier_maj.write("Nom                : " + titre + " " + Nom.get() + " " + Prenom.get() + "\nEntreprise         : " + Entreprise.get() + "\nTel                : " + Tel.get() + "\nMail               : " + Mail.get() + "\nSite               : " + Poste.get())
                fichier_maj.close()
                print ("Clé Thirard n°", recherche_cle_dispo, "libre")
                clignoled = int(recherche_cle_dispo)
                clignoled = clignoled - 1
                pixels[clignoled] = (0, 0, 255)
                time.sleep(0.5)
                pixels[clignoled] = (0, 0, 0)
                time.sleep(0.5)
                pixels[clignoled] = (0, 0, 255)
                time.sleep(0.5)
                pixels[clignoled] = (0, 0, 0)
                time.sleep(0.5)
                init_led()
                break
            if test != "" : #Si clé pas dispo
                recherche_cle_dispo = int(recherche_cle_dispo)
                recherche_cle_dispo += 1
                recherche_cle_dispo = str(recherche_cle_dispo)
            recherche_cle_dispo = int(recherche_cle_dispo)
            fichier_test.close()




        showinfo('A prêter',"Clé N°"+ma1_def)

        fenetre.destroy()


    #Creation de la fenetre de prêt
    fenetre= Toplevel()
    fenetre.title("Nouveau prêt") #Titre de la fenetre

    #Champ d'entrée du titre (M. / Mme)
    Sexe = Label(fenetre, text = 'Titre : ')
    Sexe.grid(column=0,row=0, sticky='w',pady=2)

    sex = IntVar()

    homme= Radiobutton (fenetre, text="M.", variable=sex, value=0)
    homme.grid(column=1, row=0,sticky='sw')
    femme= Radiobutton (fenetre, text="Mme", variable=sex, value=1)
    femme.grid(column=2, row=0,sticky='sw')

    #Champ entrée du nom
    cpNom = Label(fenetre, text = 'Nom : ')
    cpNom.grid(column=0, row=1, sticky='w')
    Nom=StringVar()
    Champ = Entry(fenetre, textvariable= Nom, width=31)
    Champ.grid(column=1, row=1, sticky='sw', columnspan=2, padx=10)

    #Champ d'entrée du prénom
    cpPrenom = Label(fenetre, text = 'Prénom : ',)
    cpPrenom.grid(column=0, row=2,sticky='w',pady=2)
    Prenom=StringVar()
    Champ2 = Entry(fenetre, textvariable= Prenom, width=31)
    Champ2.grid(column=1, row=2,columnspan=2)

    #Champ d'entrée de l'entreprise
    cpEntreprise = Label(fenetre, text = 'Entreprise / Service : ',)
    cpEntreprise.grid(column=0, row=3,sticky='w',pady=2)
    Entreprise=StringVar()
    Champ3 = Entry(fenetre, textvariable= Entreprise, width=31)
    Champ3.grid(column=1, row=3,columnspan=2)

    #Champ d'entrée du N° Tel
    cpTel = Label(fenetre, text = 'Tel : ')
    cpTel.grid(column=0, row=4, sticky='w',pady=2)
    Tel=StringVar()
    Champ4 = Entry(fenetre, textvariable= Tel,width=31)
    Champ4.grid(column=1, row=4,columnspan=2)

    #Champ d'entrée de l'adresse e-mail
    cpMail = Label(fenetre, text = 'Adresse mail : ')
    cpMail.grid(column=0, row=5,sticky='w',pady=2)
    Mail=StringVar()
    Champ5 = Entry(fenetre, textvariable= Mail,width=31)
    Champ5.grid(column=1, row=5,columnspan=2)

    #Champ d'entrée du poste
    cpPoste = Label(fenetre, text = 'Pour accès au site de : ')
    cpPoste.grid(column=0, row=6,sticky='w', pady=2)
    Poste=StringVar()
    Champ6 = Entry(fenetre, textvariable= Poste,width=31)
    Champ6.grid(column=1, row=6,columnspan=2)

    #Champ d'entrée commentaire
    cpCommentaires = Label(fenetre, text = 'Commentaires : ')
    cpCommentaires.grid(column=0,row=7, sticky='w',pady=2)
    Commentaires=StringVar()
    Champ7 = Entry(fenetre, textvariable= Commentaires,width=31)
    Champ7.grid(column=1, row=7, ipady=25,columnspan=2)



    #Creation des bouton Valider et Envoyer
    Envoyer= Button (fenetre, text="Valider",command=envoyer, pady=2)
    Envoyer.grid (column=1, row=11,sticky='sw', pady=20)
    Effacer= Button (fenetre, text="Réeinitialiser", command=effacer, pady=2)
    Effacer.grid (column=2, row=11,sticky='sw',pady=20)


    fenetre.mainloop()


def fonc_stock_ma1():
        def stock_ma() :
            no = str(info_ma1.get())
            fichier_etat = open(no, "r")
            etat=fichier_etat.read()
            fichier_etat.close()
            if etat=="":
                showinfo('En stock', 'La clé demandée est en stock')
                fenetre_stock_ma1.destroy()
                fenetre_stock.destroy()
            if etat !="":
                fichier_info = open(no, "r")
                info=fichier_info.read()
                fichier_etat.close()
                info_window= Toplevel()
                info_window.title("Information sur clé Thirard")
                cp_info_ma = Label(info_window, text=info, justify="left")
                cp_info_ma.grid(column=0, row=0)
                echap=Button(info_window, text="Retour",command=info_window.destroy)
                echap.grid(column=0, row=1)
                info_window.mainloop()

                fenetre_stock_ma1.destroy()
                fenetre_stock.destroy()

        fenetre_stock_ma1= Toplevel()
        fenetre_stock_ma1.title("Information sur clé Thirard") # Titre de la fenetre

        # Champ entrée du n° de moyen d'accès
        cpInfo = Label(fenetre_stock_ma1, text = 'De quel clé Thirard souhaitez vous obtenir des informations ? ')
        cpInfo.grid(column=0, row=1, sticky='w')
        info_ma1=StringVar()
        Champ = Entry(fenetre_stock_ma1, textvariable= info_ma1, width=31)
        Champ.grid(column=1, row=1, sticky='sw', columnspan=2, padx=10)

        Valider= Button (fenetre_stock_ma1, text="Valider", command=stock_ma, pady=2)
        Valider.grid (row=2, column=0,columnspan=3, sticky='ew',pady=20)
        fenetre_stock_ma1.mainloop()



############################# -- MENU PRINCIPAL -- #############################

main=Tk()
main.title("Sésame") #Titre de la fenetre principale

#Fonction informations
def infos():
    showinfo("")

#Barre de menu
menubar = Menu(main)

menu1 = Menu(menubar, tearoff=0)
menu1.add_command(label="Nouveau prêt", command=nouveau)
menu1.add_command(label="Retour")
menu1.add_separator()
menu1.add_command(label="Reinitialiser", command=initialisation)
menu1.add_separator()
menu1.add_command(label="Quitter", command=main.quit)
menubar.add_cascade(label="Fichier", menu=menu1)

menu2 = Menu(menubar, tearoff=0)
menu2.add_command(label="A propos", command=infos)
menubar.add_cascade(label="Aide", menu=menu2)

main.config(menu=menubar)


#Cadre 1

Cadre1 = Frame (main, borderwidth=2, relief=GROOVE)
Cadre1.pack(padx=30, pady = 30)

#PENSER A METTRE IMAGE DANS CE FRAME

#Cadre 2

Cadre2 = Frame (main, borderwidth=2, relief=GROOVE)
Cadre2.pack(padx=30, pady = 30)

Nouveau = Button (Cadre2, text="Nouveau prêt", command=nouveau, pady = 20, padx = 20)
Nouveau.grid (column=0, row=0)

Retour = Button (Cadre2, text="Retour de prêt", command=fonc_ret_ma1, pady = 20, padx = 20)
Retour.grid (column=1, row=0)

Etat = Button (Cadre2, text="Voir l'état du stock", command=fonc_stock_ma1, pady = 20, padx = 20)
Etat.grid (column=2, row=0)

main.mainloop()

